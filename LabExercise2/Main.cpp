#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>

using namespace std;

enum Rank 
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit 
{
	HEART,
	DIAMOND,
	SPADE,
	CLUB
};

struct Card
{
	Suit suit;
	Rank rank;
};

Suit CardSuit();
Rank CardRank();
void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	srand(time(NULL));

	Card card1;
	Card card2;

	card1.suit = CardSuit();
	card1.rank = CardRank();

	card2.suit = CardSuit();
	card2.rank = CardRank();

	PrintCard(card1);

	PrintCard(card2);

	HighCard(card1, card2);
	
	_getch();
	return 0;
}

Suit CardSuit()
{
	int gensuit = rand() % 3;

	Suit suit = HEART;
	switch (gensuit)
	{
	case 1: suit = DIAMOND;
		break;
	case 2: suit = SPADE;
		break;
	case 3: suit = CLUB;
	}

	return suit;
}

Rank CardRank()
{
	int genrank = rand() % 14 + 2;

	Rank rank = TWO;
	switch (genrank)
	{
	case 3: rank = THREE;
		break;
	case 4: rank = FOUR;
		break;
	case 5: rank = FIVE;
		break;
	case 6: rank = SIX;
		break;
	case 7: rank = SEVEN;
		break;
	case 8: rank = EIGHT;
		break;
	case 9: rank = NINE;
		break;
	case 10: rank = TEN;
		break;
	case 11: rank = JACK;
		break;
	case 12: rank = QUEEN;
		break;
	case 13: rank = KING;
		break;
	case 14: rank = ACE;
	}

	return rank;
}

void PrintCard(Card card)
{
	string rank;
	switch (card.rank)
	{
	case TWO: rank = "Two";
		break;
	case THREE: rank = "Three";
		break;
	case FOUR: rank = "Four";
		break;
	case FIVE: rank = "Five";
		break;
	case SIX: rank = "Six";
		break;
	case SEVEN: rank = "Seven";
		break;
	case EIGHT: rank = "Eight";
		break;
	case NINE: rank = "Nine";
		break;
	case TEN: rank = "Ten";
		break;
	case JACK: rank = "Jack";
		break;
	case QUEEN: rank = "Queen";
		break;
	case KING: rank = "King";
		break;
	case ACE: rank = "Ace";
	}

	string suit;
	switch (card.suit)
	{
	case HEART: suit = "Hearts";
		break;
	case DIAMOND: suit = "Diamonds";
		break;
	case SPADE: suit = "Spades";
		break;
	case CLUB: suit = "Clubs";
	}

	cout << "The " << rank << " of " << suit << "\n\n";

}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		cout << "The higher card is ";

		PrintCard(card1);
		return card1;
	}
	else if (card2.rank > card1.rank)
	{
		cout << "The higher card is ";

		PrintCard(card2);
		return card2;
	}
	else
	{
		cout << "The cards are of equal value.";
	}

}